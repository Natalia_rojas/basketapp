# Generated by Django 3.0.4 on 2020-04-13 17:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0002_auto_20200413_1708'),
    ]

    operations = [
        migrations.CreateModel(
            name='PLayer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('player_name', models.CharField(max_length=200)),
                ('choice_text', models.CharField(max_length=200)),
                ('votes', models.IntegerField(default=0)),
            ],
        ),
        migrations.DeleteModel(
            name='Choise',
        ),
    ]
