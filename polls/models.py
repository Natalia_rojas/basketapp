from django.db import models


class Team(models.Model):
    team_name = models.CharField(max_length=200)
    team_description = models.TextField() #textfiel abarca textos mas grandes
    team_logo = models.ImageField(upload_to= "Team/imageprofile/")
    team_code = models.CharField(max_length=200)
    team_coach = models.OneToOneField("Coach", on_delete = models.CASCADE) #onetoonefield es una relacion uno es a uno relacionando la tabla coach con team

    def __str__(self):
        return "%s - %s" % (self.team_name, self.team_coach.coach_name) #%s - %s el primero busca la informacion del nombre del team y el segundo el nombre del coach del team (entre parentesis)

class Player(models.Model):
    player_name = models.CharField(max_length=200)
    player_aka = models.CharField(max_length=200)
    player_date = models.DateField() #DateField para fecha
    player_age = models.PositiveIntegerField() #PositiveIntegerField Solo admite numeros positivos
    player_rut = models.CharField(max_length = 15)
    player_email = models.EmailField(max_length=254)
    player_height = models.FloatField()
    player_weight = models.FloatField()
    player_image = models.ImageField(upload_to= "player/imageprofile/")
    player_position =  models.CharField(
        max_length=3,
        choices=(
            ('BS', 'Base'),
            ('ES', 'Escolta'),
            ('ALE', 'Alero'),
            ('ALA', 'Ala-pivot'),
            ('PI', 'Pivot')), 
        default='BS') #default = valor por defecto que se ingresa a la bdd 
    team = models.ForeignKey("Team", on_delete = models.CASCADE)

    def __str__(self):
        return "%s" % (self.player_name)


class Coach(models.Model):
    coach_name = models.CharField(max_length=200)
    coach_age = models.PositiveIntegerField()
    coach_email = models.EmailField(max_length=254)
    coach_rut = models.CharField(max_length = 15)
    coach_aka = models.CharField(max_length=200)

    def __str__(self):
        return "%s" % (self.coach_name)
            
class Game(models.Model):
    game_name = models.CharField(max_length=200)

    def __str__(self):
        return "%s" % (self.game_name)


class TeamPlayer(models.Model): #LISTA DE JUGADORES QUE JUGARAN EL PARTIDO
    player = models.OneToOneField("Player", on_delete = models.CASCADE) 
    team = models.ForeignKey("Team",on_delete = models.CASCADE)

    def __str__(self):
        return "%s" % (self.team_name)

class GameList(models.Model): #CONEXION ENTRE CLASE GAME Y LISTPLAYER PARA LOS QUE LOS EQUIPOS PARTICIPEN EN DIFERENTES PARTIDOS
    game = models.OneToOneField("Game", on_delete = models.CASCADE)
    gameplayer = models.ForeignKey("TeamPlayer", on_delete = models.CASCADE)
    typeteam = models.CharField( max_length= 2,  #Atributo que muestra si el equipo es local o visita dentro del juego
        choices = 
            (('LC', 'Local'),
            ('VS', 'Visita')),
        default = 'LC')





    
        







	
		
